<?php

namespace App\Handler;

use Symfony\Component\DependencyInjection\ContainerInterface;

class JsonRpcClient
{

    const JSON_RPC_VERSION = 2.0;

    private $JSON_RPC_ULR;

    protected $container;

    const RPCSCTRCUR = [
        'jsonrpc' => self::JSON_RPC_VERSION,
        'method' => null,
        'params' => [],];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->JSON_RPC_ULR = $container->getParameter('rpcServer') . '/json-rpc';
    }

    public function jsonRpcRequest(string $method, array $params , $notify = false): array
    {
        $token = $this->getToken();
        $params['userToken'] = $token;
        $params['accessToken'] = $this->accessTokenGenerate($token);
        $requestData = self::RPCSCTRCUR;
        $requestData['method'] = $method;
        $requestData['params'] = $params;
        if (!$notify){
        $requestData['id'] = $token;
        }
        return $this->send($requestData);
    }


    private function send(array $postData): array {
        $ch = curl_init();
        $headers  = ['Content-Type: text/plain'];
        curl_setopt($ch, CURLOPT_URL,$this->JSON_RPC_ULR);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, '[' . json_encode($postData) . ']');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec ($ch);
        $result = json_decode($result,true);
        return $result ?  $result : [];
    }

    private function getToken(): string {
        if (empty($_COOKIE['user_token'])){
            $token = openssl_random_pseudo_bytes(16);
            $token = (string)bin2hex($token);
            setcookie('user_token',$token, time() + 3600 * 60);
        }else{
            $token = $_COOKIE['user_token'];
        }
        return $token;
    }

    public function accessTokenGenerate(string $userToken){
        $salt = $_ENV['APP_SALT'];
        return $salt . ":" . MD5($salt . ":" . $_ENV['APP_SECRET'] . ":" . $userToken);
    }
}