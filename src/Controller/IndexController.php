<?php

namespace App\Controller;

use App\Handler\JsonRpcClient;
use App\Services\VisitService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(VisitService $visitService)
    {
        $visitService->sendVisit();
        return $this->render('index/index.html.twig');
    }

    /**
     *
     */
    public function page1(VisitService $visitService){
        $visitService->sendVisit();
        return $this->render('index/page1.html.twig');
    }

    public function page2(VisitService $visitService){
        $visitService->sendVisit();
        return $this->render('index/page2.html.twig');
    }
}
