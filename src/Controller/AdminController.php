<?php

namespace App\Controller;

use App\Services\VisitService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @param VisitService $visitService
     * @return Response
     */
    public function index(Request $request,VisitService $visitService)
    {
        $page  = $request->get('page') ;
        $page = $page ? $page : 1;
        $visits = $visitService->getUserVisits($page);
        if (isset($visit['message'])){
            dump($visits);
            die();
        }
        return $this->render('admin/index.html.twig', ['visits' => $visits ?? []]);
    }
}
