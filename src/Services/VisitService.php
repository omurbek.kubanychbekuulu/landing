<?php

namespace App\Services;

use App\Handler\JsonRpcClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class VisitService extends JsonRpcClient
{

    private $request;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct($container);

        $this->request = Request::createFromGlobals();
    }

    public function sendVisit()
    {
        try {
            $url = $this->request->getUri();
            if ($this->request->headers->get('referer') != $url) {
                $this->jsonRpcRequest('visit', ['url' => $url], true);
            }
        } catch (\Exception $e) {
        }
    }

    public function getUserVisits($page)
    {
        return $this->jsonRpcRequest('get_visits', ['page' => $page])[0]['result'] ?? false;
    }
}